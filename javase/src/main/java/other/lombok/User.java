package other.lombok;

import lombok.Data;

/**
 * @author lszhen
 * @Description lombok的使用
 * @date 2018/5/31下午9:51
 */
@Data
public class User {
    String name;
    Integer age;
    String address;
}
