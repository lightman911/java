package design.proxy.dynamic.jdk;

import design.proxy.steady.Sell;
import design.proxy.steady.Vender;

/**
 * 动态代理测试类
 * @author Administrator
 * @date 2018/5/19 0019上午 8:11
 */
public class Client {
    public static void main(String[] args) {
        MyInvocatioHandler handler = new MyInvocatioHandler(new Vender());
        Sell sell = (Sell) handler.getProxy();
        sell.sell();
    }
}
