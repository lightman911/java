package design.proxy.dynamic.jdk;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Date;

/**
 * jdk动态代理类:和静态代理最大的区别：动态代理类只需要写一个，单静态代理类要写多个
 * @author Administrator
 * @date 2018/5/19 0019上午 7:59
 */
public class MyInvocatioHandler implements InvocationHandler {
    private Object target;

    public MyInvocatioHandler(Object target) {
        this.target = target;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Date currentDate = new Date();
        System.out.println("前置方法，执行前时间："+ currentDate);
        Object result = method.invoke(target,args);
        System.out.println("后置方法，执行后时间："+ currentDate);
        return result;
    }

    /**
     * 生成代理对象
     * @return
     */
    public Object getProxy(){
//        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        //指定类加载器
        ClassLoader loader = target.getClass().getClassLoader();
        //第二个参数要实现和目标对象一样的接口，所以只需要拿到目标对象的实现接口
        Class<?>[] interfaces = target.getClass().getInterfaces();
        //第三个参数表明这些被拦截的方法在被拦截时需要执行哪个InvocationHandler的invoke方法
        return Proxy.newProxyInstance(loader,interfaces,this);
    }

}
