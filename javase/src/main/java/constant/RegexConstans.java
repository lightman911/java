package constant;

/**
 * @author lszhen
 * @Description java常用的正则表达式
 * @date 2018/5/31上午10:00
 */
public class RegexConstans {
    /**
     * 5位数字
     */
    public static String numReg1 = "\\d{5}";

    /**
     * 3位或者4位数字：\d 相当与[0,9]
     */
    public static String numReg2 = "\\d{3}|\\d{4}";

    /**
     * 匹配带小数位的正数
     */
    public static String numReg3 = "([1,9]\\d*\\.?\\d*)|(0\\.\\d*[1,9])";

    /**
     * 简易的yyyy-MM-dd 日期格式
     */
    public static String dateReg1 = "\\d{4}-\\d{2}-\\d{2}";
}
