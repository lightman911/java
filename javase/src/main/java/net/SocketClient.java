package net;

import java.io.*;
import java.net.Socket;

/**
 * @Description Socket客户端
 * @Author Administrator
 * @Date 2018/6/9 0009上午 9:14
 */
public class SocketClient {
    public static void main(String[] args) throws Exception{
        //1、创建客户端Socket，指定服务器地址和端口
        Socket socket = new Socket("localhost",10086);
        //2、获取输出流，向服务器端发送信息
        OutputStream os = socket.getOutputStream();
        PrintWriter pw = new PrintWriter(os);
        pw.write("用户名：admin");
        pw.flush();
        socket.shutdownOutput();

        //3、获取输入流，并读取服务器端的响应信息
        InputStream is = socket.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String info = null;
        while ((info=br.readLine())!=null){
            System.out.println("我是客户端，服务器说："+info);
        }

        //4、关闭资源
        br.close();
        is.close();
        pw.close();
        socket.close();
    }
}
