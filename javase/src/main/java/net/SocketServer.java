package net;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @Description 基于TCP协议的Socket通信，实现用户登录，服务端
 * @Author wzm
 * @Date 2018/6/9 上午 8:57
 */
public class SocketServer {
    public static void main(String[] args) throws Exception{
        //1、创建一个服务器端Socket，即ServerSocket，指定绑定的端口，并监听此端口
        ServerSocket serverSocket = new ServerSocket(10086);
        //2、调用accept()方法开始监听，等待客户端的连接
        Socket socket = serverSocket.accept();
        //3、获取输入流，并读取客户端信息
        InputStream is = socket.getInputStream();
        InputStreamReader reader = new InputStreamReader(is);
        BufferedReader bfReader = new BufferedReader(reader);
        String info = null;
        while ((info=bfReader.readLine()) != null){
            System.out.println("我是服务器，客户端说"+info);
        }
        //关闭输入流
        socket.shutdownInput();
        //4、获取输出流，响应客户端的请求
        OutputStream os = socket.getOutputStream();
        PrintWriter pw = new PrintWriter(os);
        pw.write("欢迎你");
        pw.flush();

        //5、关闭资源
        pw.close();
        os.close();
        bfReader.close();
        reader.close();
        is.close();
        socket.close();
        serverSocket.close();
    }

}
