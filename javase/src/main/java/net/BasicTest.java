package net;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.URL;

/**
 * @Description java Socket常用的类测试
 * @Author wzm
 * @Date 2018/6/9 上午 8:26
 */
public class BasicTest {
    public static void main(String[] args) throws Exception {
        //net basic
        //获取本机的InetAddress实例
        InetAddress address = InetAddress.getLocalHost();
        //获取计算机名
        String hostName = address.getHostName();
        //获取IP地址
        String ipAddress = address.getHostAddress();
          System.out.println("计算机名：" + hostName + "，ip地址：" + ipAddress);

        byte[] bytes = address.getAddress();//获取字节数组形式的IP地址,以点分隔的四部分

        //获取其他主机的InetAddress实例
//      InetAddress address2 =InetAddress.getByName("其他主机名");
//      InetAddress address3 =InetAddress.getByName("IP地址");

        System.out.println("####################################");
        //URL
        //创建一个URL的实例
        URL baidu = new URL("http://www.baidu.com");
        //？表示参数，#表示锚点
        URL url = new URL(baidu, "/index.html?username=tom#test");
        System.out.println("获取协议" + url.getProtocol());
        System.out.println("获取主机" + url.getHost());

        //如果没有指定端口号，根据协议不同使用默认端口。此时getPort()方法的返回值为 -1
        System.out.println("获取端口" + url.getPort());

        System.out.println("获取文件路径" + url.getPath());
        //文件名，包括文件路径+参数
        System.out.println(url.getFile());
        //相对路径，就是锚点，即#号后面的内容
        System.out.println(url.getRef());
        //查询字符串，即参数
        System.out.println(url.getQuery());

        System.out.println("####################################");

        //使用URL读取网页内容
        //创建一个URL实例
        URL url2 = new URL("http://www.baidu.com");
        //通过openStream方法获取资源的字节输入流
        InputStream is = url2.openStream();
        //将字节输入流转换为字符输入流,如果不指定编码，中文可能会出现乱码
        InputStreamReader isr = new InputStreamReader(is, "UTF-8");
        //为字符输入流添加缓冲，提高读取效率
        BufferedReader br = new BufferedReader(isr);
        //读取数据
        String data = br.readLine();
        while (data != null) {
            //输出数据
            System.out.println(data);
            data = br.readLine();
        }
        br.close();
        isr.close();
        is.close();

        System.out.println("########################################");
    }
}
