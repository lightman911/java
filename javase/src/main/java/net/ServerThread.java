package net;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @Description 多线程处理多个客户端的连接
 * @Author Administrator
 * @Date 2018/6/9 0009下午 11:39
 */
public class ServerThread implements Runnable{

    //和本线程相关的socket
    Socket socket = null;

    //服务器处理代码
    @Override
    public void run() {
        System.out.println("服务器处理逻辑");
        try{
            InputStream is = socket.getInputStream();
            InputStreamReader reader = new InputStreamReader(is);
            BufferedReader bfReader = new BufferedReader(reader);
            String info = null;
            while ((info=bfReader.readLine()) != null){
                System.out.println("我是服务器，客户端说"+info);
            }
            //关闭输入流
            socket.shutdownInput();
            //4、获取输出流，响应客户端的请求
            OutputStream os = socket.getOutputStream();
            PrintWriter pw = new PrintWriter(os);
            pw.write("欢迎你");
            pw.flush();

            //5、关闭资源
            pw.close();
            os.close();
            bfReader.close();
            reader.close();
            is.close();
            socket.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public ServerThread(Socket socket) {
        this.socket = socket;
    }

    public static void main(String[] args) throws Exception {
        //服务器代码
        ServerSocket serverSocket = new ServerSocket(10086);
        Socket socket = null;
        int count = 0;//记录客户端的数量
        while (true) {
            socket = serverSocket.accept();
            Thread serverThread = new Thread(new ServerThread(socket));
            serverThread.start();
            count++;
            System.out.println("客户端连接的数量：" + count);
        }
    }
}
