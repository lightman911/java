package utils;

import java.util.regex.Pattern;

/**
 * @author lszhen
 * @Description 一些工具类
 * @date 2018/5/31上午10:04
 */
public class ToolUtil {
    /**
     * 判断给定的字符串是否和正则匹配
     * @param regex 正则表达式
     * @param msg 需要验证的字符串
     * @return
     */
    public static boolean matches(String regex,String msg){
        Pattern pattern = Pattern.compile(regex);
        return pattern.matcher(msg).matches();
    }
}
