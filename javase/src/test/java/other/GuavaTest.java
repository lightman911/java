package other;

import com.google.common.base.*;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * @author lszhen
 * @Description guava的用法测试
 * @date 2018/5/30下午9:55
 */
public class GuavaTest {
    public static class Student {
    }

    /**
     * 字符串合并
     */
    @Test
    public void joinTest() {
        String joinResult = Joiner.on(" ").join(new String[]{"hello", "world"});
        System.out.println(joinResult);
    }

    /**
     * map的合并
     */
    @Test
    public void doubleJoinTest() {
        Map<String, String> map = new HashMap<>();
        map.put("a", "b");
        map.put("c", "d");
        String mapJoinResult = Joiner.on(",").withKeyValueSeparator("=").join(map);
        System.out.println(mapJoinResult);
    }

    /**
     * 二次拆分
     */
    @Test
    public void doubleSplit() {
        String toSplitString = "a=b;c=d,e=f";
        Map<String, String> kvs = Splitter.onPattern("[,;]{1,}").withKeyValueSeparator('=').split(toSplitString);
        for (Map.Entry<String, String> entry : kvs.entrySet()) {
            System.out.println(String.format("%s=%s", entry.getKey(), entry.getValue()));
        }
    }

    /**
     * 字符串拆分
     */
    @Test
    public void splitTest() {
        //onPattern方法传入的是一个正则表达式,trimResults()方法表示要对结果做trim,omitEmptyStrings()表示忽略空字符串，split方法会执行拆分操作。
        Iterable<String> splitResults = Splitter.onPattern("[,，]{1,}")
                .trimResults()
                .omitEmptyStrings()
                .split("hello,word,,世界，水平");

        for (String item : splitResults) {
            System.out.println(item);
        }
    }

    /**
     * 字符串前后补全，感觉意义不大
     */
    @Test
    public void padStringTest() {
        int minLength = 4;
        String padEndResult = Strings.padEnd("123", minLength, '0');
        System.out.println("padEndResult is " + padEndResult);

        String padStartResult = Strings.padStart("1", 2, '0');
        System.out.println("padStartResult is " + padStartResult);
    }

    /**
     * 提取相同的前缀后缀
     */
    @Test
    public void commonPreficTest() {
        //Strings.commonPrefix(a,b) demo
        String a = "com.jd.coo.Hello";
        String b = "com.jd.coo.Hi";
        String ourCommonPrefix = Strings.commonPrefix(a, b);
        System.out.println("a,b common prefix is " + ourCommonPrefix);

        //Strings.commonSuffix(a,b) demo
        String c = "com.google.Hello";
        String d = "com.jd.Hello";
        String ourSuffix = Strings.commonSuffix(c, d);
        System.out.println("c,d common suffix is " + ourSuffix);
    }

    /**
     * 字符串判断是否为空
     */
    @Test
    public void isNullOrEmptyTest() {
        String input = "";
        boolean isNullOrEmpty = Strings.isNullOrEmpty(input);
        System.out.println("input " + (isNullOrEmpty ? "is " : "is not ") + "empty");
    }

    /**
     * 对象比较
     */
    @Test
    public void objEqualTest() {
        Object a = null;
        Object b = new Object();
        boolean aEqualsB = Objects.equal(a, b);
        System.out.println(aEqualsB);
    }

    /**
     * null快速失败，避免空指针异常,这时候程序会第一时间抛出空指针异常，这可以帮助我们尽早发现问题。
     */
    @Test
    public void optinalTest() {
        Optional<Integer> possible = Optional.of(6);
        if (possible.isPresent()) {
            System.out.println("possible isPresent:" + possible.isPresent());
            System.out.println("possible value:" + possible.get());
        }
    }

    @Test
    public void optinalTest2() {
        Optional<Student> possibleNull = Optional.of(null);
        possibleNull.get();
    }

    @Test
    public void optinaleTest3() {
        // Optional.absent方法来初始化posibleNull实例
        Optional<Student> possibleNull = Optional.absent();
        Student jim = possibleNull.get();
    }


}
