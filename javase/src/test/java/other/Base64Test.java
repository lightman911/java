package other;

import org.junit.Test;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Base64;

/**
 * @author lszhen
 * @Description base64的编码和解码处理
 * @date 2018/5/29上午10:58
 */
public class Base64Test {
    /**
     * 若因版本问题不能使用1.8的方式，则使用此方式
     */
    @Test
    public void apache_commons_codecTest() throws UnsupportedEncodingException {
        String text = "测试字符串";
        byte[] textByte = text.getBytes("UTF-8");
        org.apache.commons.codec.binary.Base64 base64 = new org.apache.commons.codec.binary.Base64();
        //编码
        String encodedText = base64.encodeToString(textByte);
        System.out.println(encodedText);
        //解码
        System.out.println(new String(base64.decode(encodedText), "UTF-8"));
    }

    /**
     * 编码解码效率不高，后期可能不支持，不建议使用
     * @throws IOException
     */
    @Test
    public void jdk7Test() throws IOException {
        BASE64Encoder encoder = new BASE64Encoder();
        BASE64Decoder decoder = new BASE64Decoder();
        String text = "测试字符串";
        byte[] textByte = text.getBytes("UTF-8");
        //编码
        String encodeText = encoder.encode(textByte);
        System.out.println("jdk1.7编码:" + encodeText);
        //解码
        String decodeText = new String(decoder.decodeBuffer(encodeText), "UTF-8");
        System.out.println("jdk1.7解码："+decodeText);
    }

    /**
     * 推荐使用1.8的方式进行base64的编码，解码
     * @throws UnsupportedEncodingException
     */
    @Test
    public void jdk8Test() throws UnsupportedEncodingException {
        String text = "测试字符串";
        byte[] textByte = text.getBytes("UTF-8");
        //编码
        String encodeText = Base64.getEncoder().encodeToString(textByte);
        System.out.println("jdk1.8编码:" + encodeText);
        //解码
        String decodeText = new String(Base64.getDecoder().decode(encodeText), "UTF-8");
        System.out.println("jdk1.8解码："+decodeText);
    }
}
