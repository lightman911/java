package other;

import constant.RegexConstans;
import org.junit.Assert;
import org.junit.Test;
import utils.ToolUtil;

/**
 * @author lszhen
 * @Description java正则表达式
 * @date 2018/5/31上午9:56
 */
public class RegexTest {
    @Test
    public void testNum1() {
        String msg = "1234";
        String regex = RegexConstans.numReg1;
        String errMsg = "匹配失败，msg不是5位数字,msg长度=" + msg.length();

        Assert.assertEquals(errMsg, true, ToolUtil.matches(regex, msg));
    }

    @Test
    public void testNum2() {
        String msg = "12345";
        String regex = RegexConstans.numReg2;
        String errMsg = "匹配失败，msg不是3位或者4位数字,msg长度=" + msg.length();

        Assert.assertEquals(errMsg, true, ToolUtil.matches(regex, msg));
    }

    @Test
    public void testNum3() {
        String msg = "0.0";
        String regex = RegexConstans.numReg3;
        String errMsg = "匹配失败，msg=" + msg;

        Assert.assertEquals(errMsg, true, ToolUtil.matches(regex, msg));
    }

    @Test
    public void test3() {
        String msg = "2017-2-4";
        String regex = RegexConstans.dateReg1;
        String errMsg = "匹配失败，日期格式必须是yyyy-MM-dd,当前日期=" + msg;

        Assert.assertEquals(errMsg, true, ToolUtil.matches(regex, msg));
    }
}
