package com.lszhen.mybatis.service;

import com.lszhen.mybatis.entity.ErrorInfo;
import com.lszhen.mybatis.excption.MyException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.servlet.http.HttpServletRequest;

/**
 * @author lszhen
 * @Description ${todo}
 * @Title ${file_name}
 * @Package ${package_name}
 * @date 2018/6/4下午2:45
 */
@ControllerAdvice
@EnableWebMvc
public class GloableExceptionHandler {


    @ExceptionHandler(value = MyException.class)
    @ResponseBody
    public ErrorInfo jsonErrorHandler(HttpServletRequest req, MyException e) throws Exception{
        ErrorInfo error = new ErrorInfo();
        error.setCode(e.getErrCode());
        error.setMsg(e.getErrMsg());
        error.setUrl(req.getRequestURI());
        return error;
    }
}
