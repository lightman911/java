package com.lszhen.mybatis.controller;

import com.lszhen.mybatis.dao.UserDao;
import com.lszhen.mybatis.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lszhen
 * @Description
 * @date 2018/5/31下午3:49
 */
@RestController
public class UserController {
    @Autowired
    private UserDao userDao;

    /**
     * @return {"name":"wzm","password":"123"}
     */
    @RequestMapping("/getUser")
    public User getUser() {
        User user = new User();
        user.setUserName("wzm");
        user.setPassword("123");
        return user;
    }

    /**
     * 描述：@PathVariable:从url中获取参数值
     *
     * @param userName
     * @return
     */
    @RequestMapping("/getwzm/{userName}")
    //value:指的是缓存的名字
//    @Cacheable(value = "user-key")
    public User getUser2(@PathVariable String userName) {
        User user = userDao.getByUserName(userName);
        System.out.println("若下面没出现“无缓存的时候调用”字样且能打印出数据表示测试成功");
        return user;
    }

    /**
     * 描述：@RequestParam请求参数规则注解，value匹配前台传递的参数
     *
     * @param userName
     * @return
     */
    @RequestMapping("/getUser3")
    public User getUser3(@RequestParam(value = "name", defaultValue = "lszhen") String userName) {
        User user = userDao.getByUserName(userName);
        System.out.println("若下面没出现“无缓存的时候调用”字样且能打印出数据表示测试成功");
        return user;
    }

    /**
     * 测试事务的回滚，有异常就回滚，这条数据不会插入成功，一般@Transactional注解放在service层，这里只是为了方便放在controller
     */
    @RequestMapping("/testRollBack")
    @Transactional(rollbackFor = Exception.class)
    public void insert() {
        User user = new User();
        user.setUserName("wzm1");
        user.setPassword("1234");
        userDao.insert(user);
        String a = null;
        a.indexOf("c");
    }
}
