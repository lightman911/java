package com.lszhen.mybatis.dao;

import com.lszhen.mybatis.entity.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;

/**
 * 定义user的各个dao方法
 * Created by lszhen on 2017/11/19.
 */
public interface UserRepository extends JpaRepository<User,Integer> {
    User findByUserName(String userName);

    List<User> findByUserName(String wzm, Pageable pageable);

    List<User> findByUserNameOrId(String userName, int id);

    List<User> findByUserNameAndId(String userName, int id);

    /**
     * 可以自定义查询
     * @param id
     * @return
     */
    @Transactional
    @Modifying
    @Query("delete from User where id=?1")
    int deleteByUserId(int id);
}

