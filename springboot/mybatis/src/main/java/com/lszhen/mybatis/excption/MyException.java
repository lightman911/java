package com.lszhen.mybatis.excption;

/**
 * @author lszhen
 * @Description 自定义异常
 * @date 2018/6/4下午2:39
 */
public class MyException extends RuntimeException {
    public MyException(){
        super();
    }

    public MyException(String errCode, String errMsg) {
        this.errCode = errCode;
        this.errMsg = errMsg;
    }

    private String errCode;
    private String errMsg;

    public String getErrCode() {
        return errCode;
    }

    public void setErrCode(String errCode) {
        this.errCode = errCode;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }
}
