package com.lszhen.mybatis.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lszhen
 * @Description 测试log文件的写入
 */
@RestController
@RequestMapping("log")
public class LogController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @RequestMapping("writelog")
    public Object writeLog()
    {
        StringBuilder errMsg = new StringBuilder("This is an error message");
        for (int i=0;i<10;i++){
            errMsg.append(errMsg);
        }

        logger.debug("This is a debug message");
        logger.info("This is an info message");
        logger.warn("This is a warn message");
        logger.error(errMsg.toString());
//        new LogHelper().helpMethod();
        return "OK";
    }
}
