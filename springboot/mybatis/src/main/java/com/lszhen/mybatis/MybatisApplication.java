package com.lszhen.mybatis;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author lszhen
 * @Description springBoot和mybatis整合，包括事务
 * @date 2018/5/31下午2:19
 */
@EnableTransactionManagement//开启事务管理
@MapperScan("com.lszhen.mybatis.dao")//这样就不需要在每个mapper上加@Mapper注解了
@SpringBootApplication
public class MybatisApplication {
    public static void main(String[] args) {
        SpringApplication.run(MybatisApplication.class,args);
    }
}
