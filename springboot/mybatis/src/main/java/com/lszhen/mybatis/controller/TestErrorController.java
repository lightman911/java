package com.lszhen.mybatis.controller;

import com.lszhen.mybatis.excption.MyException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lszhen
 * @Description 测试异常返回的处理方式
 * @date 2018/6/4下午2:36
 */
@RestController
public class TestErrorController {

    @RequestMapping("/returnHtml")
    public String testErrorReturn1() throws Exception{
        throw new Exception("请求出错");
    }

    @RequestMapping("/returnJson")
    public String testErrorReturn2() throws MyException{
        System.out.println("123");
        throw new MyException("123","请求出错2");
    }
}
