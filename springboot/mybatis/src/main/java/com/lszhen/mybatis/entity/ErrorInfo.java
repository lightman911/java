package com.lszhen.mybatis.entity;

/**
 * @author lszhen
 * @Description ${todo}
 * @Title ${file_name}
 * @Package ${package_name}
 * @date 2018/6/4下午2:50
 */
public class ErrorInfo {
    private String code;
    private String msg;
    private String url;

    public void setCode(String code) {
        this.code = code;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
