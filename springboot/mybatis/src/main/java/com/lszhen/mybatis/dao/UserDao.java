package com.lszhen.mybatis.dao;

import com.lszhen.mybatis.entity.User;
import org.apache.ibatis.annotations.*;

/**
 * @author lszhen
 * @Description 通过注解的方式写sql：@Result修饰返回的结果集，如果实体类属性名和table字段名相同，可以省略
 * @date 2018/5/31下午4:39
 */
public interface UserDao {
    /**
     * 根据用户名称查找
     * @param userName
     * @return
     */
    @Select("SELECT * FROM USER WHERE user_name = #{userName}")
    @Results({
            @Result(property = "userName",column = "user_name"),
    })
    User getByUserName(String userName);

    /**
     * 根据id查找
     * @param id
     * @return
     */
    @Select("SELECT * FROM USER WHERE id = #{id}")
    @Results({
            @Result(property = "userName",column = "user_name"),
    })
    User getById(int id);

    /**
     * 查询所有
     * @return
     */
    @Select("SELECT * FROM USER")
    @Results({
            @Result(property = "userName",column = "user_name"),
    })
    User getAll();

    /**
     * 插入数据
     * @param user
     */
    @Insert("INSERT INTO USER(USER_NAME,PASSWORD) VALUES(#{userName},#{password})")
    void insert(User user);

    /**
     * 更新数据
     * @param user
     */
    @Update("UPDATE USER SET USER_NAME=#{userName},PASSWORD=#{password} WHERE ID=#{id}")
    void update(User user);

    /**
     * 根据id删除数据
     * @param id
     */
    @Delete("DELETE FROM USER WHERE ID=#{id}")
    void delete(int id);
}
