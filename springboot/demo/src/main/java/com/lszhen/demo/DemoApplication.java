package com.lszhen.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lszhen
 * @Description 通用的启动写法
 * @date 2018/5/29上午9:08
 */
@RestController
@SpringBootApplication
public class DemoApplication {
    //注入配置文件中的值
    @Value("${book.author}")
    private String author;
    @Value("${book.name}")
    private String name;

    @Autowired
    private AutoSetting autoSetting;

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class,args);
    }

    /**
     * 测试读取配置文件中的值
     * @return
     */
    @RequestMapping("/")
    public String index(){
        return "book.author = "+author+",book.name = "+name;
    }

    /**
     * 效果和上面一样
     * @return
     */
    @RequestMapping("/book")
    public String getIndex2(){
        return "book.author = "+autoSetting.getAuthor()+",book.name = "+autoSetting.getName();
    }

}
