package com.lszhen.demo;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author lszhen
 * @Description 将配置文件和bean关联，实现类型安全的配置,避免用@Value注入多次
 * -@ConfigurationProperties：加载配置文件，prefix指定properties里面的的前缀，location指定配置文件的位置
 *
 * @date 2018/5/29下午4:29
 */
@Component
@ConfigurationProperties(prefix = "book")
public class AutoSetting {
    private String author;
    private String name;

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
